package dk.itu.mario.engine.JFugue;

import java.util.ArrayList;
import java.util.HashMap;

import org.jfugue.elements.Layer;
import org.jfugue.elements.Note;

public class LayeredNote {
	long time;
	ArrayList<Note> SimultaneousNotes = new ArrayList<Note>();
	
	public LayeredNote(long time, ArrayList<Note> SimNotes)
	{
		if(!SimNotes.isEmpty())
		{
			this.time = time;
			SimultaneousNotes.addAll(SimNotes);
		}
	}

	/**
	 * @return the simultaneousNotes
	 */
	public ArrayList<Note> getSimultaneousNotes() {
		return SimultaneousNotes;
	}

	/**
	 * @param simultaneousNotes the simultaneousNotes to set
	 */
	public void setSimultaneousNotes(ArrayList<Note> simultaneousNotes) {
		SimultaneousNotes = simultaneousNotes;
	}
	
	public Note getNote(int index)
	{
		return SimultaneousNotes.get(index);
	}
	
	public void appendNote(Note note)
	{
		SimultaneousNotes.add(note);
	}
	
	public Note findNote(int noteValue)
	{
		for(Note note : SimultaneousNotes)
		{
			if(note.getValue() == noteValue)
			{
				return new Note(note);
			}
		}
		
		Note note = new Note();
		note.setRest(true);
		note.setDecimalDuration(0.25);
		
		return note;
	}
	
	public LayeredNote findSimultaneousNotes(int noteValue)
	{
		ArrayList<Note> SimultaneousNotes = new ArrayList<Note>();
		SimultaneousNotes.add(findNote(noteValue));
		return new LayeredNote(this.getTime(), SimultaneousNotes);
	}

	/**
	 * @return the time
	 */
	public long getTime() {
		return time;
	}

	/**
	 * @param time the time to set
	 */
	public void setTime(long time) {
		this.time = time;
	}
	
	public String toString()
	{
		String result = "";
		
		result += time + " : ";
		
		for(Note note : SimultaneousNotes)
		{
			result += note.getMusicString() + "[" + note.getValue() + "] ";
		}
		
		return result;
	}
	
	public String toMusicString()
	{
		String result = " ";
		
		for(int index = 0; index < SimultaneousNotes.size(); index++)
		{
			Layer layer = new Layer((byte)index);
			Note note = new Note(SimultaneousNotes.get(index));
			result += /*layer.getMusicString() + " " +*/ note.getMusicString() + " ";
		}
		
		return result;
	}
	
	public static String PercNameFromValue(int value)
	{	
		switch(value)
		{
			case 35:
				return "ACOUSTIC_BASE_DRUM";
			case 36:
				return "BASS_DRUM";
			case 37:
				return "SIDE_KICK";
			case 38:
				return "ACOUSTIC_SNARE";
			case 39:
				return "HAND_CLAP";
			case 40:
				return "ELECTRIC_SNARE";
			case 41:
				return "LOW_FLOOR_TOM";
			case 42:
				return "CLOSED_HI_HAT";
			case 43:
				return "HIGH_FLOOR_TOM";
			case 44:
				return "PEDAL_HI_TOM";
			case 45:
				return "LOW_TOM";
			case 46:
				return "OPEN_HI_HAT";
			case 47:
				return "LOW_MID_TOM";
			case 48:
				return "HI_MID_TOM";
			case 49:
				return "CRASH_CYMBAL_1";
			case 50:
				return "HIGH_TOM";
			case 51:
				return "RIDE_CYMBAL_1";
			case 52:
				return "CHINESE_CYMBAL";
			case 53:
				return "RIDE_BELL";
			case 54:
				return "TAMBOURINE";
			case 55:
				return "SPLASH_CYMBAL";
			case 56:
				return "COWBELL";
			case 57:
				return "CRASH_CYMBAL_2";
			case 58:
				return "VIBRASLAP";
			case 59:
				return "RIDE_CYMBAL_2";
			case 60:
				return "HI_BONGO";
			case 61:
				return "LOW_BONGO";
			case 62:
				return "MUTE_HI_CONGA";
			case 63:
				return "OPEN_HI_CONGA";
			case 64:
				return "LOW_CONGO";
			case 65:
				return "HIGH_TIMBALE"; 
			case 66:
				return "LOW_TIMBALE"; 
			case 67:
				return "HIGH_AGOGO"; 
			case 68:
				return "LOW_AGOGO"; 
			case 69:
				return "CABASA";
			case 70:
				return "MARACAS";
			case 71:
				return "SHORT_WHISTLE";
			case 72:
				return "LONG_WHISTLE";
			case 73:
				return "SHORT_GUIRO";
			case 74:
				return "LONG_GUIRO";
			case 75: 
				return "CLAVES";
			case 76:
				return "HI_WOOD_BLOCK";
			case 77:
				return "LOW_WOOD_BLOCK";
			case 78:
				return "MUTE_CUICA";
			case 79:
				return "OPEN_CUICA";
			case 80:
				return "MUTE_TRIANGLE";
			case 81: 
				return "OPEN_TRIANGLE";
				
			default:
			{
				return "UNKNOWN";
			}
		}
	}
	
	public static int PercValueFromName(String name)
	{	
		if(name.equalsIgnoreCase("ACOUSTIC_BASE_DRUM"))
		{
			return 35;
		}
		else if(name.equalsIgnoreCase("BASS_DRUM"))
		{
			return 36;
		}
		else if(name.equalsIgnoreCase("ACOUSTIC_SNARE"))
		{
			return 38;
		}
		else if(name.equalsIgnoreCase("LOW_FLOOR_TOM"))
		{
			return 41;
		}
		else if(name.equalsIgnoreCase("CLOSED_HI_HAT"))
		{
			return 42;
		}
		else if(name.equalsIgnoreCase("HIGH_FLOOR_TOM"))
		{
			return 43;
		}
		else if(name.equalsIgnoreCase("PEDAL_HI_TOM"))
		{
			return 44;
		}
		else if(name.equalsIgnoreCase("LOW_TOM"))
		{
			return 45;
		}
		else if(name.equalsIgnoreCase("OPEN_HI_HAT"))
		{
			return 46;
		}
		else if(name.equalsIgnoreCase("LOW_MID_TOM"))
		{
			return 47;
		}
		else if(name.equalsIgnoreCase("HI_MID_TOM"))
		{
			return 48;
		}
		else if(name.equalsIgnoreCase("CRASH_CYMBAL_1"))
		{
			return 49;
		}
		else if(name.equalsIgnoreCase("HIGH_TOM"))
		{
			return 50;
		}
		else if(name.equalsIgnoreCase("RIDE_CYMBAL_1"))
		{
			return 51;
		}
		else if(name.equalsIgnoreCase("CHINESE_CYMBAL"))
		{
			return 52;
		}
		else if(name.equalsIgnoreCase("RIDE_BELL"))
		{
			return 53;
		}
		else if(name.equalsIgnoreCase("ELECTRIC_SNARE"))
		{
			return 40;
		}
		else if(name.equalsIgnoreCase("SPLASH_CYMBAL"))
		{
			return 55;
		}
		else if(name.equalsIgnoreCase("RIDE_CYMBAL_2"))
		{
			return 59;
		}
		else if(name.equalsIgnoreCase("COWBELL"))
		{
			return 56;
		}
		else if(name.equalsIgnoreCase("CRASH_CYMBAL_2"))
		{
			return 57;
		}
		else if(name.equalsIgnoreCase("SIDE_KICK"))
		{
			return 37;
		}
		else if(name.equalsIgnoreCase("HAND_CLAP"))
		{
			return 39;
		}
		else if(name.equalsIgnoreCase("TAMBOURINE"))
		{
			return 54;
		}
		else if(name.equalsIgnoreCase("VIBRASLAP"))
		{
			return 58;
		}
		else if(name.equalsIgnoreCase("HI_BONGO"))
		{
			return 60;
		}
		else if(name.equalsIgnoreCase("LOW_BONGO"))
		{
			return 61;
		}
		else if(name.equalsIgnoreCase("MUTE_HI_CONGA"))
		{
			return 62;
		}
		else if(name.equalsIgnoreCase("OPEN_HI_CONGA"))
		{
			return 63;
		}
		else if(name.equalsIgnoreCase("LOW_CONGO"))
		{
			return 64;
		}
		else if(name.equalsIgnoreCase("HIGH_TIMBALE"))
		{
			return 65;
		}
		else if(name.equalsIgnoreCase("LOW_TIMBALE"))
		{
			return 66;
		}
		else if(name.equalsIgnoreCase("HIGH_AGOGO"))
		{
			return 67;
		}
		else if(name.equalsIgnoreCase("LOW_AGOGO"))
		{
			return 68;
		}
		else if(name.equalsIgnoreCase("CABASA"))
		{
			return 69;
		}
		else if(name.equalsIgnoreCase("MARACAS"))
		{
			return 70;
		}
		else if(name.equalsIgnoreCase("SHORT_WHISTLE"))
		{
			return 71;
		}
		else if(name.equalsIgnoreCase("LONG_WHISTLE"))
		{
			return 72;
		}
		else if(name.equalsIgnoreCase("SHORT_GUIRO"))
		{
			return 73;
		}
		else if(name.equalsIgnoreCase("LONG_GUIRO"))
		{
			return 74;
		}
		else if(name.equalsIgnoreCase("CLAVES"))
		{
			return 75;
		}
		else if(name.equalsIgnoreCase("HI_WOOD_BLOCK"))
		{
			return 76;
		}
		else if(name.equalsIgnoreCase("LOW_WOOD_BLOCK"))
		{
			return 77;
		}
		else if(name.equalsIgnoreCase("MUTE_CUICA"))
		{
			return 78;
		}
		else if(name.equalsIgnoreCase("OPEN_CUICA"))
		{
			return 79;
		}
		else if(name.equalsIgnoreCase("MUTE_TRIANGLE"))
		{
			return 80;
		}
		else if(name.equalsIgnoreCase("OPEN_TRIANGLE"))
		{
			return 81;
		}
		else
		{
			return 0;
		}
	}
}
