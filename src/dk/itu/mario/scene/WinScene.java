package dk.itu.mario.scene;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;

import dk.itu.mario.MarioInterface.GamePlay;
import dk.itu.mario.engine.JFugue.JFugueMidiImporter;
import dk.itu.mario.engine.sprites.Mario;

import dk.itu.mario.engine.MarioComponent;
import dk.itu.mario.engine.Art;

public class WinScene extends Scene
{
    private int tick;
    private String scrollMessage = "Thank you for saving me, Mario!";
//    
//    private int difficulty;
//    private int richness;
//    private GamePlay gp;
    
    private MarioComponent marioComponent;

    public WinScene()
    {
    	super();
    }

    public void init()
    {
    }

    public void render(Graphics g, float alpha)
    {
        g.setColor(Color.decode("#8080a0"));
        g.fillRect(0, 0, 320, 240);
        g.drawImage(Art.endScene[tick/24%2][0], 160-48, 100-48, null);
        drawString(g, scrollMessage, 160-scrollMessage.length()*4, 160, 0);
    }

    private void drawString(Graphics g, String text, int x, int y, int c)
    {
        char[] ch = text.toCharArray();
        for (int i = 0; i < ch.length; i++)
        {
            g.drawImage(Art.font[ch[i] - 32][c], x + i * 8, y, null);
        }
    }

    private boolean wasDown = true;
    public void tick()
    {
        tick++;
        if (!wasDown && keys[Mario.KEY_JUMP])
        {
//            component.toTitle();
        }
        if (keys[Mario.KEY_JUMP])
        {
            wasDown = false;
        }
    }

    public float getX(float alpha)
    {
        return 0;
    }

    public float getY(float alpha)
    {
        return 0;
    }

	@Override
	public void mouseClicked(MouseEvent me) {
////		reset(false, gp, difficulty, richness);
//		this.marioComponent.toCustomGame();
	}
	
//	public void setResetParams(MarioComponent marioComponent, GamePlay gp, int difficulty, int richness)
//	{
//		this.marioComponent = marioComponent;
//		this.difficulty = difficulty;
//		this.richness = richness;
//		this.gp = gp;
//	}
	
	@Override
	public void setMIDIImport(JFugueMidiImporter midiImport,
			long totalMIDITime, long totalLevelTime) {
		// TODO Auto-generated method stub
		
	}
}
