package dk.itu.mario.level.generator;

import java.util.Random;
import java.util.ArrayList;

import dk.itu.mario.MarioInterface.Constraints;
import dk.itu.mario.MarioInterface.GamePlay;
import dk.itu.mario.MarioInterface.LevelGenerator;
import dk.itu.mario.MarioInterface.LevelInterface;
import dk.itu.mario.level.CustomizedLevel;
import dk.itu.mario.level.MyLevel;

import dk.itu.mario.engine.JFugue.*;

public class MyLevelGenerator extends CustomizedLevelGenerator implements LevelGenerator{

	public LevelInterface generateLevel(GamePlay playerMetrics, JFugueMidiImporter midiImport, int difficulty, int richness) {
		
//		MusicParserForMarioLevelGen tempParserMario = new MusicParserForMarioLevelGen(midiImport.getNoteListForVoice(0));
//		int time = (int) (tempParserMario.getTotalTime() / 1000);
//		time = (time != 0) ? time : 320;
//		
//		System.out.println(time + ", " + time * 4);
		
		LevelInterface level = new MyLevel(320, 20,new Random().nextLong(),difficulty, richness,LevelInterface.TYPE_OVERGROUND,playerMetrics, midiImport);
        
        //LevelInterface level = new CustomizedLevel(320,15,new Random().nextLong(),1,LevelInterface.TYPE_OVERGROUND,playerMetrics);
        
		return level;
	}

	@Override
	public LevelInterface generateLevel(String detailedInfo) {
		// TODO Auto-generated method stub
		return null;
	}

}
