package dk.itu.mario.MarioInterface;

public class PlayerType {
	
	public static final int PLAYER_TYPE_KILLER = 0;
	public static final int PLAYER_TYPE_COLLECTOR = 1;
	public static final int PLAYER_TYPE_EXPLORER = 2;
	public static final int PLAYER_TYPE_SOCIALIZER = 3;
	public static final int PLAYER_TYPE_RANDOM = 4;
	
	public static final double KILLER_THRESHOLD = 80.0;
	public static final double COLLECTOR_THRESHOLD = 80.0;
	public static final double EXPLORER_THRESHOLD = 80.0;
	public static final double SOCIALIZER_THRESHOLD = 80.0;
	
	GamePlay gp;
	
	double KillerValue;
	double ExplorerValue;
	double CollectorValue;
	double SocializerValue;
	
	PlayerType()
	{
		
	}
	
	public PlayerType(GamePlay gp)
	{
		this.gp = gp;
		
		if(gp.totalEnemies > 0)
			KillerValue = (double)gp.totalKilled() / (double)gp.totalEnemies * 100f;
		else
			KillerValue = 0;
		
		if(gp.totalCoins > 0)
			CollectorValue = (double)gp.coinsCollected / (double)gp.totalCoins * 100f;
		else
			CollectorValue = 0;
		
		
		//TODO Find Heuristic for detecting Explorer Type
		ExplorerValue = 0;
		
		//Not Applicable To Mario
		SocializerValue = 0;
	}
	
	public int getPlayerType()
	{
		if(isUniqueType())
		{
			if(isKiller())
			{
				return PLAYER_TYPE_KILLER;
			}
			else if(isCollector())
			{
				return PLAYER_TYPE_COLLECTOR;
			}
			else if(isExplorer())
			{
				return PLAYER_TYPE_EXPLORER;
			}
			else if(isSocializer())
			{
				return PLAYER_TYPE_SOCIALIZER;
			}
		}
		else
		{
			//Assuming we're only checking for Killer and Collector types
			if(KillerValue >= CollectorValue && isKiller())
			{
				return PLAYER_TYPE_KILLER;
			}
			else if(CollectorValue > KillerValue && isCollector())
			{
				return PLAYER_TYPE_COLLECTOR;
			}
		}
		
		return PLAYER_TYPE_RANDOM;
	}
	
	public boolean isKiller()
	{
		return KillerValue > KILLER_THRESHOLD;
	}
	
	public boolean isCollector()
	{
		return CollectorValue > COLLECTOR_THRESHOLD;
	}
	
	public boolean isExplorer()
	{
		return ExplorerValue > EXPLORER_THRESHOLD;
	}
	
	public boolean isSocializer()
	{
		return SocializerValue > SOCIALIZER_THRESHOLD;
	}
	
	public boolean isRandom()
	{
		return (!isKiller() && !isExplorer() && !isCollector() && !isSocializer());
	}
	
	public boolean isUniqueType()
	{
		int countType = 0;
		if(isKiller())
		{
			countType++;
		}
		if(isExplorer())
		{
			countType++;
		}
		if(isCollector())
		{
			countType++;
		}
		if(isSocializer())
		{
			countType++;
		}
		
		return (countType == 1 || countType == 0);
	}

	/**
	 * @return the killerValue
	 */
	public double getKillerValue() {
		return KillerValue;
	}

	/**
	 * @param killerValue the killerValue to set
	 */
	public void setKillerValue(double killerValue) {
		KillerValue = killerValue;
	}

	/**
	 * @return the collectorValue
	 */
	public double getCollectorValue() {
		return CollectorValue;
	}

	/**
	 * @param collectorValue the collectorValue to set
	 */
	public void setCollectorValue(double collectorValue) {
		CollectorValue = collectorValue;
	}
	
	
}
